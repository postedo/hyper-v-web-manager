﻿using System.Web.Mvc;
using HyperVWebManager.Models.ViewModel;

namespace HyperVWebManager.Controllers
{
  public class ManageController : Controller
  {
    //
    // GET: /Vm/
    public ActionResult Index()
    {
      var switchController = new Api.SwitchController();
      var vm = new ManageIndexViewModel
      {
        Vms = Api.VmController.GetVms(),
        Switches = switchController.Get()
      };
      return View(vm);
    }
  }
}