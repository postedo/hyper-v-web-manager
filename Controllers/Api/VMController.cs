﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Management;
using System.Web.Http;
using HyperVWebManager.Helpers;
using HyperVWebManager.Models;
using System.Linq;

namespace HyperVWebManager.Controllers.Api
{
  [RoutePrefix("api/vm")]
  public class VmController : ApiController
  {
    private const string StartFormat = "$vm = Get-VM -ComputerName {0} -Id {1}\r\nif ($vm.State -eq \"Paused\")\r\n{{\r\n    Resume-VM $vm\r\n}}\r\nelse\r\n{{\r\n    Start-VM $vm\r\n}}";
    private const string StopFormat = "$vm = Get-VM -ComputerName {0} -Id {1}\r\nStop-VM $vm –TurnOff";
    private const string ShutDownFormat = "$vm = Get-VM -ComputerName {0} -Id {1}\r\nStop-VM $vm";
    private const string PauseFormat = "$vm = Get-VM -ComputerName {0} -Id {1}\r\nSuspend-VM $vm";
    private const string SaveFormat = "$vm = Get-VM -ComputerName {0} -Id {1}\r\nSave-VM $vm";


    private static ConnectionOptions _connectionOptions;

    private static ConnectionOptions DefaultConnectionOptions
    {
      get
      {
        return _connectionOptions ?? (_connectionOptions = new ConnectionOptions
        {
          Authentication = AuthenticationLevel.PacketPrivacy,
          Impersonation = ImpersonationLevel.Impersonate
        });
      }
    }

    [Route("")]
    public IEnumerable<Vm> Get()
    {
      return GetVms();
    }

    [Route("{id}")]
    public Vm GetSpecific(Guid id)
    {
        var vms = GetVms();
        return vms.FirstOrDefault(vm => vm.Id == id);
    }

    [Route("start"), HttpPost]
    public string StartVm(VmInfo info)
    {
      return PSHelper.RunScript(string.Format(StartFormat, info.HyperVHost, info.Id));
    }

    [Route("stop"), HttpPost]
    public string StopVm(VmInfo info)
    {
      return PSHelper.RunScript(string.Format(StopFormat, info.HyperVHost, info.Id));
    }

    [Route("shutdown"), HttpPost]
    public string ShutdownVm(VmInfo info)
    {
      return PSHelper.RunScript(string.Format(ShutDownFormat, info.HyperVHost, info.Id));
    }

    [Route("pause"), HttpPost]
    public string PauseVm(VmInfo info)
    {
      return PSHelper.RunScript(string.Format(PauseFormat, info.HyperVHost, info.Id));
    }


    [Route("save"), HttpPost]
    public string SaveVm(VmInfo info)
    {
      return PSHelper.RunScript(string.Format(SaveFormat, info.HyperVHost, info.Id));
    }

    public static IEnumerable<Vm> GetVms()
    {
      string computerName = Environment.MachineName;
      computerName = "localhost";
      List<ManagementObject> vms = GetAllVmsFromComputerName(computerName);
      foreach (ManagementObject managementObject in vms)
      {
        Vm vm = new Vm { Name = managementObject.SystemProperties["ElementName"].Value as string };
        var elemName = managementObject.SystemProperties["Name"].Value.ToString();
        Guid id;
        var parsed = Guid.TryParse(elemName, out id);
        if (parsed)
        {
          vm.Id = id;
          var date = (string)managementObject.SystemProperties["InstallDate"].Value;
          vm.CreationTime = Parsedate(date);
          vm.Description = managementObject.SystemProperties["Description"].Value as string;
          vm.State = (VmState)managementObject.SystemProperties["EnabledState"].Value;
          var uptime = (ulong)managementObject.SystemProperties["OnTimeInMilliseconds"].Value;
          vm.Uptime = TimeSpan.FromMilliseconds(uptime);
          yield return vm;
        }
      }
    }

    private static DateTime Parsedate(string date)
    {
      return DateTime.ParseExact(date.Substring(0, "yyyyMMddHHmmss.ffffff".Length), "yyyyMMddHHmmss.ffffff", CultureInfo.CurrentCulture);
    }

    private static List<ManagementObject> GetAllVmsFromComputerName(string computerName)
    {
      GetManagementScopeForComputer(computerName);
      string query = "select * from Msvm_ComputerSystem";
      return GetVmByQuery(computerName, query);
    }

    private static ManagementScope GetManagementScopeForComputer(string computerName)
    {
      if (string.IsNullOrEmpty(computerName))
        return new ManagementScope("root\\virtualization\\v2", DefaultConnectionOptions);
      return new ManagementScope(string.Format(CultureInfo.InstalledUICulture, "\\\\{0}\\root\\virtualization\\v2", new object[1]
      {
        computerName
      }), DefaultConnectionOptions);
    }

    private static List<ManagementObject> GetVmByQuery(string computerName, string query)
    {
      ObjectQuery query1 = new ObjectQuery(query);
      ManagementScope scopeForComputer = GetManagementScopeForComputer(computerName);
      List<ManagementObject> list = new List<ManagementObject>();
      using (ManagementObjectSearcher managementObjectSearcher = new ManagementObjectSearcher(scopeForComputer, query1))
      {
        foreach (ManagementObject managementObject in managementObjectSearcher.Get())
        {
          if (managementObject != null)
          {
            string right = (string)managementObject["Name"];
            string nameFromFullName = GetNetBiosNameFromFullName(computerName);
            if (CompareString(computerName, right) != 0 && CompareString(nameFromFullName, right) != 0)
              list.Add(managementObject);
          }
        }
        list.Sort(new Comparison<ManagementObject>(CompareVmByName));
        return list;
      }
    }

    private static int CompareString(string left, string right)
    {
      int num = 0;
      if (left == null && right == null)
        return num;
      if (left == null)
        return -1;
      if (right == null)
        return 1;
      return string.Compare(left, right, StringComparison.OrdinalIgnoreCase);
    }

    private static int CompareVmByName(ManagementObject left, ManagementObject right)
    {
      return CompareWmiObject(left, right, "ElementName");
    }

    private static int CompareWmiObject(ManagementObject left, ManagementObject right, string propertyName)
    {
      int num = 0;
      if (left == null)
      {
        if (right != null)
          num = -1;
        return num;
      }
      if (right == null)
        return 1;
      return string.Compare((string)left.GetPropertyValue(propertyName), (string)right.GetPropertyValue(propertyName), StringComparison.CurrentCultureIgnoreCase);
    }

    private static string GetNetBiosNameFromFullName(string fullName)
    {
      string str = fullName;
      if (string.IsNullOrEmpty(fullName))
        return str;
      int length = fullName.IndexOf('.');
      if (length != -1)
        str = fullName.Substring(0, length);
      if (str.Length > 15)
        str = str.Substring(0, 15);
      return str.ToUpperInvariant();
    }
  }
}
