﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Management.Automation;
using System.Management.Automation.Runspaces;
using System.Web.Http;
using HyperVWebManager.Helpers;
using HyperVWebManager.Models;

namespace HyperVWebManager.Controllers.Api
{
  [RoutePrefix("api/switch")]
  public class SwitchController : ApiController
  {
    private const string HyperVHost = "localhost";

    private IEnumerable<Switch> RunScript(string scriptText)
    {
      Runspace runspace = RunspaceFactory.CreateRunspace();
      runspace.Open();
      Pipeline pipeline = runspace.CreatePipeline();
      pipeline.Commands.AddScript(scriptText);
      Collection<PSObject> results = pipeline.Invoke();
      runspace.Close();
      foreach (PSObject psObject in results)
      {
        Switch ret = new Switch();
        Microsoft.HyperV.PowerShell.VMSwitch sw = (Microsoft.HyperV.PowerShell.VMSwitch)psObject.ImmediateBaseObject;
        ret.Name = sw.Name;
        ret.Id = sw.Id;
        ret.Notes = sw.Notes;
        ret.SwitchType = sw.SwitchType.ToString("G");
        ret.HyperVHost = HyperVHost;
        ret.NetAdapterInterfaceDescription = sw.NetAdapterInterfaceDescription;
        yield return ret;
      }
    }

    [Route("")]
    public IEnumerable<Switch> Get()
    {
      return RunScript(string.Format("Get-VMSwitch -ComputerName {0}", HyperVHost));
    }

    [Route("remove"), HttpPost]
    public IEnumerable<Switch> Remove(Switch sw)
    {
      PSHelper.RunScript(string.Format("$sw = Get-VMSwitch -Id {0}\r\nRemove-VMSwitch $sw -Force", sw.Id));
      return Get();
    }

    [Route("new"), HttpPost]
    public IEnumerable<Switch> New(Switch sw)
    {
      string scriptText = string.Empty;
      if (new string[2]
      {
        "Private",
        "Internal"
      }.Contains(sw.SwitchType))
        scriptText = string.Format("New-VMSwitch -Name {1} -ComputerName {0} -Notes {2} -SwitchType {3}", sw.HyperVHost, sw.Name, sw.Notes, sw.SwitchType);
      else if (!string.IsNullOrWhiteSpace(sw.NetAdapterInterfaceDescription))
        scriptText = string.Format("New-VMSwitch -Name {1} -ComputerName {0} -Notes {2} -InterfaceDecription {3}", sw.HyperVHost, sw.Name, sw.Notes, sw.NetAdapterInterfaceDescription);
      if (!string.IsNullOrWhiteSpace(scriptText))
        PSHelper.RunScript(scriptText);
      return this.Get();
    }
  }
}
