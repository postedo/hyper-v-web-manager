﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HyperVWebManager.Models.ViewModel
{
  public class ManageIndexViewModel
  {
    public IEnumerable<Vm> Vms { get; set; }
    public IEnumerable<Switch> Switches { get; set; } 
  }
}