﻿using System;

namespace HyperVWebManager.Models
{
  public class Switch
  {
    public Guid Id;
    public string Name;
    public string Notes;
    public string SwitchType;
    public string HyperVHost;
    public string NetAdapterInterfaceDescription;
  }
}
