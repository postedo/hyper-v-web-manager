﻿namespace HyperVWebManager.Models
{
  public enum VmState : ushort
  {
    Unknown = (ushort) 0,
    Enabled = (ushort) 2,
    Disabled = (ushort) 3,
    Saved = (ushort) 6,
    Paused = (ushort) 9,
    Suspended = (ushort) 32769,
    Starting = (ushort) 10,
    Snapshotting = (ushort) 32771,
    Saving = (ushort) 32773,
    Stopping = (ushort) 32774,
    Pausing = (ushort) 32776,
    Resuming = (ushort) 32777,
  }
}
