﻿using System;

namespace HyperVWebManager.Models
{
  public class VmInfo
  {
    public Guid Id;
    public string HyperVHost;
  }
}
