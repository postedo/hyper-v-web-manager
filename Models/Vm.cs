﻿using System;

namespace HyperVWebManager.Models
{
  public class Vm
  {
    public DateTime CreationTime { get; set; }

    public string Description { get; set; }

    public DateTime InstallDate { get; set; }

    public string Name { get; set; }

    public Guid Id { get; set; }

    public VmState State { get; set; }

    public string EnabledStateString
    {
      get
      {
        return this.State.ToString("G");
      }
    }

    public TimeSpan Uptime { get; set; }

    public string UptimeString
    {
      get
      {
        return this.Uptime.ToString("g");
      }
    }
  }
}
