﻿// Decompiled with JetBrains decompiler
// Type: HyperVWeb.PSHelper
// Assembly: HyperVWeb, Version=0.1.1.0, Culture=neutral, PublicKeyToken=null
// MVID: A10D8571-4F90-4F6E-B109-4A8B32FBA506
// Assembly location: C:\Users\edo\Downloads\Setup_HyperVWeb_0.1.1.0\HyperVWeb.exe

using System.Collections.ObjectModel;
using System.Management.Automation;
using System.Management.Automation.Runspaces;
using System.Text;

namespace HyperVWebManager.Helpers
{
  public static class PSHelper
  {
    public static string RunScript(string scriptText)
    {
      Runspace runspace = RunspaceFactory.CreateRunspace();
      runspace.Open();
      Pipeline pipeline = runspace.CreatePipeline();
      pipeline.Commands.AddScript(scriptText);
      Collection<PSObject> collection = pipeline.Invoke();
      runspace.Close();
      StringBuilder stringBuilder = new StringBuilder();
      foreach (PSObject psObject in collection)
        stringBuilder.AppendLine(psObject.ToString());
      return stringBuilder.ToString();
    }
  }
}
